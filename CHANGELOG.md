# Changelog for the Optimyzer API Client

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.5.1] - 2023-04-17

### Fixed

- Fixed typos in README
- Deployment to PyPI
- Relaxed test comparison to 1e-3

## [0.5.0] - 2022-11-30

### Added

- Methods to get all the results and the best result of an optimization
- A jupyter notebook with a simple Optimyzer API tutorial for HPO

### Changed

- The `optimizations` property of `Machine` is now a `get_all_optimizations` method

## [0.4.0] - 2022-11-27

### Added

- An Optimization class to handle API interactions for running optimizations
- A Setting class to help create new optimizations
- Methods for getting suggestions and reporting results

### Changed

- An optimization can now be created and selected within a Machine object

## [0.3.0] - 2022-11-22

### Added

- A Machine class to handle API interactions with machines
- A `machine` property to `OptimyzerClient` to store a selected optimization

### Changed

- Renamed `Optimyzer` to `OptimyzerClient` for better readability
- Moved API functionality into an `OptimyzerConnection` module
- Removed the preceding `/` from all endpoints (added once to the API url)

### Removed

- The `base` test, which just tested importing `optimyzer_api_client`

## [0.2.0] - 2022-11-20

### Added

- Functionality for creating and accessing machines

## [0.1.0] - 2022-11-20

### Added

- An initial `Optimyzer` API Client class with login functionality
- A secret loader script to get sensitive information from a file or environment variable
- A coverage config file

## [0.0.1] - 2022-11-20

### Added

- all basic repository files including a readme, changelog and pipeline
