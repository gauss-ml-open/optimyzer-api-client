{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# HPO using Optimyzer\n",
    "\n",
    "First we need to import the client."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%load_ext autoreload\n",
    "%autoreload 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from optimyzer_api_client import OptimyzerClient, Setting, SettingType"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create an instance of the Optimyzer API Client\n",
    "To create an instance, you can hard-code your username and password into your code, or you can save the data into a credentials file.\n",
    "\n",
    "The credentials file should look like this:\n",
    "\n",
    "```json\n",
    "{\n",
    "    \"OPTIMYZER_USERNAME\": \"your@email.com\",\n",
    "    \"OPTIMYZER_PASSWORD\": \"a_very_str0ng_p@ssw0rd!\",\n",
    "}\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# oac = OptimyzerClient.from_login(username=\"your@email.com\", password=\"a_very_str0ng_p@ssw0rd!\")\n",
    "oac = OptimyzerClient.from_credentials(\"../secrets/user_credentials.json\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating and accessing machines\n",
    "Machines provide a useful way of clustering optimizations for knowledge transfer.\n",
    "For example, many optimizations are run on a laser cutting machine for different materials.\n",
    "Optimyzer transfers the experience from previous optimizations in a machine to each new\n",
    "optimization created.\n",
    "\n",
    "The variables indicate what changes between optimizations. For example, that can be the\n",
    "material type and material thickness.\n",
    "Each variable has a name and a dictionary containing its `type` and `units` (optional).\n",
    "\n",
    "### Example\n",
    "We will create a \"machine\" to run optimizations of hyper-parameters for NN models to classify images of hand-written digits of the MNIST dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "new_id = oac.add_machine(\n",
    "    \"HPO MNIST\", \"A test machine for HPO of a NN to classify images of hand-written digits\", {\"# hidden layers\": {\"type\": \"num\"}}\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[{'uid': '63874ca4fcd0155f643dea20', 'name': 'HPO MNIST'}]\n"
     ]
    }
   ],
   "source": [
    "# With this method you can get an overview of all the machines in the user's group\n",
    "print(oac.get_all_machines())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's select a machine and create/run an optimization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 51,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "HPO MNIST - A test machine for HPO of a NN to classify images of hand-written digits\n",
      "Created on: 2022-11-30T12:29:24.260035\n"
     ]
    }
   ],
   "source": [
    "oac.select_machine(new_id)\n",
    "# oac.select_machine(\"63874ca4fcd0155f643dea20\")\n",
    "print(f\"{oac.machine.name} - {oac.machine.description}\")\n",
    "print(f\"Created on: {oac.machine.created_on}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating and accessing optimizations\n",
    "An optimization can be defined in 4 parts.\n",
    "\n",
    "### Part 1 - the \"constants\"\n",
    "Here we give the optimization a name, a description and give the machine `variables` a constant value.\n",
    "\n",
    "### Part 2 - the settings\n",
    "Here we define which settings we want the AI to optimize, their type and a relevant search range.\n",
    "\n",
    "### Part 3 - the measurements\n",
    "Here we define the feedback that will be provided after each experiment. This can be a single measurement, some subjective feedback or anything else.\n",
    "Notes can already be added to each experiment.\n",
    "\n",
    "### Part 4 - the goal\n",
    "Here we let Optimyzer know what we would like to maximize.\n",
    "This is usually a weighted sum of relevant settings and measurements.\n",
    "Larger weights indicate more importance of that setting/measurement.\n",
    "\n",
    "If you want to optimize a more complex response function, you can add its result as a measurment and just maximize that measurement.\n",
    "\n",
    "### Example\n",
    "Let's optimize the number of hidden units in each layer and the dropout probability"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 60,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[{'uid': '638756c7fcd0155f643deadb',\n",
       "  'name': 'Model #1',\n",
       "  'variables': {'# hidden layers': 3.0}}]"
      ]
     },
     "execution_count": 60,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "oac.machine.get_all_optimizations()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 53,
   "metadata": {},
   "outputs": [],
   "source": [
    "settings = [\n",
    "    Setting(\"# of units p/layer\", SettingType.DISCRETE, [20, 30, 40, 50, 60, 70, 80]),\n",
    "    Setting(\"dropout prob.\", SettingType.FLOAT, [0.01, 0.15])\n",
    "]\n",
    "\n",
    "measurements = {\"performance\": {\"values\": [0.85, 0.98]}}\n",
    "\n",
    "goal_weights = {\"performance\": (1, {\"# of units p/layer\": -0.2})}\n",
    "\n",
    "new_opt_id = oac.machine.add_optimization(\n",
    "    \"Model #1\",\n",
    "    \"The first model with 3 layers and ReLU activation\",\n",
    "    {\"# hidden layers\": 3},\n",
    "    settings,\n",
    "    measurements,\n",
    "    goal_weights\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 54,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Model #2\n",
      "--------\n",
      "Another model with 6 layers and ReLU activation\n",
      "# hidden layers: 6.0\n",
      "\n",
      "Settings:\n",
      "  # of units p/layer: [40, 60, 80, 100, 120] \n",
      "  dropout prob.: [0.01, 0.15] \n",
      "\n",
      "Measurements:\n",
      "  performance: [0.85, 0.98] \n",
      "\n",
      "Goal Weights:\n",
      "  performance: 1\n",
      "  └ # of units p/layer: -0.2\n"
     ]
    }
   ],
   "source": [
    "oac.machine.select_optimization(new_opt_id)\n",
    "# oac.machine.select_optimization(\"6382176abcf88bb9c837ee72\")\n",
    "print(oac.machine.optimization)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "NOTE: You'll have to refresh the `oac.machine` object if you want to see the new optimization in the list of optimizations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running an Optimization\n",
    "\n",
    "### Getting a Suggestion\n",
    "We can get a suggestion from the AI right after creating the optimization.\n",
    "Calling `get_suggestion` will return the suggested settings to try for the experiment.\n",
    "You will likely also need the settings that you defined for the machine variables, which are constant for this optimization: `optimization.variables`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 55,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'# of units p/layer': 40.0, 'dropout prob.': 0.068}\n"
     ]
    }
   ],
   "source": [
    "optimization = oac.machine.optimization\n",
    "settings = optimization.get_suggestion()\n",
    "settings[\"dropout prob.\"] = np.round(settings[\"dropout prob.\"], 3)\n",
    "print(settings)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'# hidden layers': 6.0}"
      ]
     },
     "execution_count": 56,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "optimization.variables"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also get more info from the AI by asking to include the AI's prediction for the suggested settings and the nearest data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'# of units p/layer': 60,\n",
       " 'dropout prob.': 0.022920116817144502,\n",
       " '_prediction': {'expected': 0.0, 'uncertainty': 1.0},\n",
       " '_nearest': []}"
      ]
     },
     "execution_count": 38,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "optimization.get_suggestion(include_prediction=True, include_nearest=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reporting Results\n",
    "\n",
    "Now would be a good time to install **TensorFlow** if you haven't done that. Simply run `pip install tensorflow` in your console, with the relevant conda environment active.\n",
    "\n",
    "The function below can be used to train a NN on the MNIST dataset and test its performance. The inputs to the function are the number of layers, the number of units per layer, and the dropout probability."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "metadata": {},
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "\n",
    "def train_nn(num_layers: int, num_units: int, dropout_prob: float) -> float:\n",
    "    # The MNIST dataset is already included in Keras.\n",
    "    mnist = tf.keras.datasets.mnist\n",
    "\n",
    "    # A little bit of preprocessing\n",
    "    (x_train, y_train), (x_test, y_test) = mnist.load_data()\n",
    "    # Normalize the input data to [0, 1]\n",
    "    x_train, x_test = x_train / 255.0, x_test / 255.0\n",
    "\n",
    "    # Building a simple model: N hidden layers, with dropout\n",
    "    layers = [\n",
    "        tf.keras.layers.Flatten(\n",
    "            input_shape=(28, 28)\n",
    "        ),  # (28, 28) is the size of the input images\n",
    "    ]\n",
    "    for _ in range(num_layers):  # Here we use the frozen parameter\n",
    "        layers += [\n",
    "            tf.keras.layers.Dense(num_units),  # Here we use the first parameter\n",
    "            tf.keras.layers.Dropout(dropout_prob),  # and here the second\n",
    "        ]\n",
    "    layers += [\n",
    "        tf.keras.layers.Dense(10),  # 10 is the number of classes (the digits 0-9 in MNIST)\n",
    "    ]\n",
    "    model = tf.keras.models.Sequential(layers)\n",
    "\n",
    "    # We're using an off-the-shelf loss function for classification\n",
    "    loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)\n",
    "\n",
    "    # Here the training is happening. This may take a while...\n",
    "    model.compile(optimizer=\"adam\", loss=loss_fn, metrics=[\"accuracy\"])\n",
    "    model.fit(x_train, y_train, epochs=5)  # we're running ADAM for 5 epochs\n",
    "\n",
    "    # We can now evaluate the performance of our trained model\n",
    "    performance = model.evaluate(x_test, y_test, verbose=2)[1]\n",
    "    print(\n",
    "        f\"The performance for a model with {num_layers} layers, {num_units} units per layer \"\n",
    "        f\"and a dropout probability of {dropout_prob * 100:2.2f}% was: {performance * 100:2.2f}%.\"\n",
    "    )\n",
    "    return np.round(performance, 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's run an experiment using the suggested settings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Epoch 1/5\n",
      "1875/1875 [==============================] - 5s 2ms/step - loss: 0.4276 - accuracy: 0.8745\n",
      "Epoch 2/5\n",
      "1875/1875 [==============================] - 4s 2ms/step - loss: 0.3581 - accuracy: 0.8983\n",
      "Epoch 3/5\n",
      "1875/1875 [==============================] - 4s 2ms/step - loss: 0.3436 - accuracy: 0.9021\n",
      "Epoch 4/5\n",
      "1875/1875 [==============================] - 4s 2ms/step - loss: 0.3371 - accuracy: 0.9043\n",
      "Epoch 5/5\n",
      "1875/1875 [==============================] - 5s 2ms/step - loss: 0.3320 - accuracy: 0.9062\n",
      "313/313 - 1s - loss: 0.3194 - accuracy: 0.9115 - 573ms/epoch - 2ms/step\n",
      "The performance for a model with 3 layers, 80 units per layer and a dropout probability of 10.50% was: 91.15%.\n"
     ]
    }
   ],
   "source": [
    "performance = train_nn(\n",
    "    num_layers=int(optimization.variables[\"# hidden layers\"]),\n",
    "    num_units=int(settings[\"# of units p/layer\"]),\n",
    "    dropout_prob=settings[\"dropout prob.\"]\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can report back the results to the API with the `report_results` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 45,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = {\"performance\": performance}\n",
    "optimization.report_results(settings, results, \"First experiment\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also continue running the optimization with a simple loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 57,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Epoch 1/5\n",
      "1875/1875 [==============================] - 6s 3ms/step - loss: 0.5151 - accuracy: 0.8454\n",
      "Epoch 2/5\n",
      "1875/1875 [==============================] - 5s 2ms/step - loss: 0.4133 - accuracy: 0.8804\n",
      "Epoch 3/5\n",
      "1875/1875 [==============================] - 5s 2ms/step - loss: 0.3949 - accuracy: 0.8868\n",
      "Epoch 4/5\n",
      "1875/1875 [==============================] - 5s 2ms/step - loss: 0.3825 - accuracy: 0.8901\n",
      "Epoch 5/5\n",
      "1875/1875 [==============================] - 4s 2ms/step - loss: 0.3753 - accuracy: 0.8938\n",
      "313/313 - 1s - loss: 0.3211 - accuracy: 0.9102 - 507ms/epoch - 2ms/step\n",
      "The performance for a model with 6 layers, 40 units per layer and a dropout probability of 6.70% was: 91.02%.\n",
      "Epoch 1/5\n",
      "1875/1875 [==============================] - 8s 3ms/step - loss: 0.4464 - accuracy: 0.8697\n",
      "Epoch 2/5\n",
      "1875/1875 [==============================] - 6s 3ms/step - loss: 0.3837 - accuracy: 0.8900\n",
      "Epoch 3/5\n",
      "1875/1875 [==============================] - 7s 3ms/step - loss: 0.3649 - accuracy: 0.8948\n",
      "Epoch 4/5\n",
      "1875/1875 [==============================] - 6s 3ms/step - loss: 0.3537 - accuracy: 0.8983\n",
      "Epoch 5/5\n",
      "1875/1875 [==============================] - 6s 3ms/step - loss: 0.3472 - accuracy: 0.9006\n",
      "313/313 - 1s - loss: 0.3361 - accuracy: 0.9046 - 622ms/epoch - 2ms/step\n",
      "The performance for a model with 6 layers, 120 units per layer and a dropout probability of 1.00% was: 90.46%.\n",
      "Epoch 1/5\n",
      "1875/1875 [==============================] - 11s 5ms/step - loss: 0.5640 - accuracy: 0.8327\n",
      "Epoch 2/5\n",
      "1875/1875 [==============================] - 9s 5ms/step - loss: 0.4778 - accuracy: 0.8604\n",
      "Epoch 3/5\n",
      "1875/1875 [==============================] - 10s 5ms/step - loss: 0.4503 - accuracy: 0.8702\n",
      "Epoch 4/5\n",
      "1875/1875 [==============================] - 9s 5ms/step - loss: 0.4503 - accuracy: 0.8719\n",
      "Epoch 5/5\n",
      "1875/1875 [==============================] - 10s 5ms/step - loss: 0.4402 - accuracy: 0.8734\n",
      "313/313 - 1s - loss: 0.3802 - accuracy: 0.8891 - 830ms/epoch - 3ms/step\n",
      "The performance for a model with 6 layers, 120 units per layer and a dropout probability of 15.00% was: 88.91%.\n",
      "Epoch 1/5\n",
      "1875/1875 [==============================] - 6s 3ms/step - loss: 0.6617 - accuracy: 0.7940\n",
      "Epoch 2/5\n",
      "1875/1875 [==============================] - 5s 3ms/step - loss: 0.5182 - accuracy: 0.8475\n",
      "Epoch 3/5\n",
      "1875/1875 [==============================] - 5s 3ms/step - loss: 0.4910 - accuracy: 0.8582\n",
      "Epoch 4/5\n",
      "1875/1875 [==============================] - 5s 3ms/step - loss: 0.4742 - accuracy: 0.8635\n",
      "Epoch 5/5\n",
      "1875/1875 [==============================] - 5s 3ms/step - loss: 0.4650 - accuracy: 0.8656\n",
      "313/313 - 1s - loss: 0.3206 - accuracy: 0.9068 - 565ms/epoch - 2ms/step\n",
      "The performance for a model with 6 layers, 40 units per layer and a dropout probability of 15.00% was: 90.68%.\n",
      "Epoch 1/5\n",
      "1875/1875 [==============================] - 6s 3ms/step - loss: 0.4193 - accuracy: 0.8763\n",
      "Epoch 2/5\n",
      "1875/1875 [==============================] - 5s 3ms/step - loss: 0.3471 - accuracy: 0.8997\n",
      "Epoch 3/5\n",
      "1875/1875 [==============================] - 5s 3ms/step - loss: 0.3313 - accuracy: 0.9040\n",
      "Epoch 4/5\n",
      "1875/1875 [==============================] - 5s 3ms/step - loss: 0.3257 - accuracy: 0.9069\n",
      "Epoch 5/5\n",
      "1875/1875 [==============================] - 5s 3ms/step - loss: 0.3217 - accuracy: 0.9080\n",
      "313/313 - 1s - loss: 0.2987 - accuracy: 0.9157 - 614ms/epoch - 2ms/step\n",
      "The performance for a model with 6 layers, 40 units per layer and a dropout probability of 1.00% was: 91.57%.\n",
      "Epoch 1/5\n",
      "1875/1875 [==============================] - 8s 4ms/step - loss: 0.4894 - accuracy: 0.8553\n",
      "Epoch 2/5\n",
      "1875/1875 [==============================] - 7s 4ms/step - loss: 0.4174 - accuracy: 0.8789\n",
      "Epoch 3/5\n",
      "1875/1875 [==============================] - 8s 4ms/step - loss: 0.4026 - accuracy: 0.8851\n",
      "Epoch 4/5\n",
      "1875/1875 [==============================] - 8s 4ms/step - loss: 0.3923 - accuracy: 0.8885\n",
      "Epoch 5/5\n",
      "1875/1875 [==============================] - 8s 4ms/step - loss: 0.3884 - accuracy: 0.8894\n",
      "313/313 - 1s - loss: 0.3415 - accuracy: 0.9007 - 718ms/epoch - 2ms/step\n",
      "The performance for a model with 6 layers, 80 units per layer and a dropout probability of 7.50% was: 90.07%.\n",
      "Epoch 1/5\n",
      "1875/1875 [==============================] - 8s 4ms/step - loss: 0.4284 - accuracy: 0.8734\n",
      "Epoch 2/5\n",
      "1875/1875 [==============================] - 7s 4ms/step - loss: 0.3691 - accuracy: 0.8942\n",
      "Epoch 3/5\n",
      "1875/1875 [==============================] - 7s 4ms/step - loss: 0.3509 - accuracy: 0.8994\n",
      "Epoch 4/5\n",
      "1875/1875 [==============================] - 7s 4ms/step - loss: 0.3459 - accuracy: 0.9002\n",
      "Epoch 5/5\n",
      "1875/1875 [==============================] - 7s 4ms/step - loss: 0.3358 - accuracy: 0.9045\n",
      "313/313 - 1s - loss: 0.3253 - accuracy: 0.9087 - 717ms/epoch - 2ms/step\n",
      "The performance for a model with 6 layers, 80 units per layer and a dropout probability of 1.00% was: 90.87%.\n",
      "Epoch 1/5\n",
      "1875/1875 [==============================] - 8s 4ms/step - loss: 0.5766 - accuracy: 0.8267\n",
      "Epoch 2/5\n",
      "1875/1875 [==============================] - 7s 4ms/step - loss: 0.4712 - accuracy: 0.8637\n",
      "Epoch 3/5\n",
      "1875/1875 [==============================] - 7s 4ms/step - loss: 0.4497 - accuracy: 0.8698\n",
      "Epoch 4/5\n",
      "1875/1875 [==============================] - 7s 4ms/step - loss: 0.4375 - accuracy: 0.8734\n",
      "Epoch 5/5\n",
      "1875/1875 [==============================] - 7s 4ms/step - loss: 0.4346 - accuracy: 0.8755\n",
      "313/313 - 1s - loss: 0.3537 - accuracy: 0.9027 - 690ms/epoch - 2ms/step\n",
      "The performance for a model with 6 layers, 80 units per layer and a dropout probability of 15.00% was: 90.27%.\n",
      "Epoch 1/5\n",
      "1875/1875 [==============================] - 13s 6ms/step - loss: 0.4975 - accuracy: 0.8530\n",
      "Epoch 2/5\n",
      "1875/1875 [==============================] - 11s 6ms/step - loss: 0.4279 - accuracy: 0.8770\n",
      "Epoch 3/5\n",
      "1875/1875 [==============================] - 11s 6ms/step - loss: 0.4151 - accuracy: 0.8807\n",
      "Epoch 4/5\n",
      "1875/1875 [==============================] - 11s 6ms/step - loss: 0.4096 - accuracy: 0.8825\n",
      "Epoch 5/5\n",
      "1875/1875 [==============================] - 12s 6ms/step - loss: 0.4024 - accuracy: 0.8834\n",
      "313/313 - 1s - loss: 0.3426 - accuracy: 0.9088 - 947ms/epoch - 3ms/step\n",
      "The performance for a model with 6 layers, 120 units per layer and a dropout probability of 7.50% was: 90.88%.\n",
      "Epoch 1/5\n",
      "1875/1875 [==============================] - 7s 3ms/step - loss: 0.4213 - accuracy: 0.8762\n",
      "Epoch 2/5\n",
      "1875/1875 [==============================] - 6s 3ms/step - loss: 0.3474 - accuracy: 0.8992\n",
      "Epoch 3/5\n",
      "1875/1875 [==============================] - 6s 3ms/step - loss: 0.3336 - accuracy: 0.9042\n",
      "Epoch 4/5\n",
      "1875/1875 [==============================] - 6s 3ms/step - loss: 0.3270 - accuracy: 0.9066\n",
      "Epoch 5/5\n",
      "1875/1875 [==============================] - 6s 3ms/step - loss: 0.3195 - accuracy: 0.9088\n",
      "313/313 - 1s - loss: 0.3156 - accuracy: 0.9145 - 645ms/epoch - 2ms/step\n",
      "The performance for a model with 6 layers, 40 units per layer and a dropout probability of 1.10% was: 91.45%.\n"
     ]
    }
   ],
   "source": [
    "for i in range(10):\n",
    "    # Get a new suggestion\n",
    "    settings = optimization.get_suggestion()\n",
    "    settings[\"dropout prob.\"] = np.round(settings[\"dropout prob.\"], 3)\n",
    "\n",
    "    # Train the NN and test the performance\n",
    "    performance = train_nn(\n",
    "        num_layers=int(optimization.variables[\"# hidden layers\"]),\n",
    "        num_units=int(settings[\"# of units p/layer\"]),\n",
    "        dropout_prob=settings[\"dropout prob.\"]\n",
    "    )\n",
    "\n",
    "    # Report the results and repeat\n",
    "    results = {\"performance\": performance}\n",
    "    optimization.report_results(settings, results, f\"Experiment #{i+2}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Checking the optimization results\n",
    "\n",
    "After running an optimization, we can check the best result obtained with the `get_best` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'uid': '63875aebfcd0155f643deb5a',\n",
       " 'settings': {'# of units p/layer': 40.0,\n",
       "  'dropout prob.': 0.01,\n",
       "  '# hidden layers': 6.0},\n",
       " 'environment': {},\n",
       " 'notes': 'Experiment #6',\n",
       " 'results': {'performance': 0.9157},\n",
       " 'score': 0.5053846153846152,\n",
       " 'user_email': 'client_test@gauss-ml.com',\n",
       " 'created_on': '2022-11-30T14:30:19.140970'}"
      ]
     },
     "execution_count": 58,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "optimization.get_best()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Overview of the Client's methods\n",
    "\n",
    "| Object | Method | Usage |\n",
    "| --- | :-: | --- |\n",
    "| OptimyzerClient | from_credentials | Create an `OptimyzerClient` instance from a credentials file |\n",
    "| OptimyzerClient | get_all_machines | Get a list of all machines belonging to the user's group, including their `uid` |\n",
    "| OptimyzerClient | add_machine | Add a new machine with the given name, description and variables |\n",
    "| OptimyzerClient | select_machine | Select the machine with the corresponding `uid` |\n",
    "| Machine | get_all_optimizations | Get a list of all the optimizations in the selected machine |\n",
    "| Machine | add_optimization | Add a new optimization. See above for detailed instructions |\n",
    "| Machine | select_optimization | Select the optimization with the corresponding `uid` |\n",
    "| Optimization | get_suggestion | Get a new suggestion to try in the next experiment |\n",
    "| Optimization | report_results | Create an experiment report including the settings used and the results measured |\n",
    "| Optimization | get_data | Get all the experiments in the optimization |\n",
    "| Optimization | get_best | Get the best experiment in the optimization |\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.10.8 ('optimyzer_api_client')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  },
  "orig_nbformat": 4,
  "vscode": {
   "interpreter": {
    "hash": "3cf6aec9985b8d1e8b0e71b3a43cb456b41ce0ffff0f7fa1bf5df63d6dc134ba"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
