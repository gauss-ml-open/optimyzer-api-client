# Copyright (c) 2020-2023, Gauss Machine Learning GmbH. All rights reserved.
# This file is part of the Optimyzer API Client, which is released under the BSD 3-Clause License.

# type: ignore
# pylint: disable=missing-module-docstring
# pylint: disable=protected-access

import pytest
from optimyzer_api_client import OptimyzerClient


def test_login():
    with pytest.raises(RuntimeError):
        OptimyzerClient.from_login("wrong@email.com", "password")

    oac = OptimyzerClient.from_credentials("secrets/user_credentials.json")
