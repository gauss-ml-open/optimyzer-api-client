# Copyright (c) 2020-2023, Gauss Machine Learning GmbH. All rights reserved.
# This file is part of the Optimyzer API Client, which is released under the BSD 3-Clause License.

# type: ignore
# pylint: disable=missing-module-docstring
# pylint: disable=protected-access

import json
import pytest


def test_get_secrets():
    from optimyzer_api_client.secret_loader import get_secrets

    secrets = get_secrets(
        "secrets/test_secrets.json",
        {"SECRET1": (str, None), "SECRET2": (int, None), "SECRET3": (float, 3.14)},
    )

    assert secrets["SECRET2"] == 42

    secrets = get_secrets(
        "tests/secrets_test.json",
        {"SECRET1": (str, None), "SECRET2": (bool, None)},
    )
    assert secrets["SECRET2"]

    # Fail if a secret without a default is missing
    with pytest.raises(RuntimeError):
        secrets = get_secrets("secrets/test_secrets.json", {"SECRET4": (str, None)})

    # Fail if a secret has the wrong type
    with pytest.raises(ValueError):
        secrets = get_secrets("secrets/test_secrets.json", {"SECRET1": (int, None)})
        secrets = get_secrets("secrets/test_secrets.json", {"SECRET2": (str, None)})


def test_json_type():
    from optimyzer_api_client.secret_loader import JSONType

    data = {"string": "abcd", "number": 3.14, "bool": True}

    assert isinstance(JSONType(data), dict)
    assert isinstance(JSONType(json.dumps(data).encode("utf-8")), dict)
