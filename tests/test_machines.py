# Copyright (c) 2020-2023, Gauss Machine Learning GmbH. All rights reserved.
# This file is part of the Optimyzer API Client, which is released under the BSD 3-Clause License.

# type: ignore
# pylint: disable=missing-module-docstring
# pylint: disable=protected-access

import numpy as np
import pytest

from optimyzer_api_client import OptimyzerClient


def get_new_machine_name(oac: OptimyzerClient):
    all_names = [machine["name"] for machine in oac.get_all_machines()]
    while True:
        new_name = f"Machine #{np.random.choice(10000)}"
        if new_name not in all_names:
            return new_name


def test_add_machine():
    oac = OptimyzerClient.from_credentials("secrets/user_credentials.json")

    res = oac.add_machine(
        get_new_machine_name(oac),
        "A simple test machine",
        {"weight": {"type": "num", "units": "kg"}, "surface": {"type": "cat"}},
    )
    assert isinstance(res, str)


def test_get_all_machines():
    oac = OptimyzerClient.from_credentials("secrets/user_credentials.json")
    new_id = oac.add_machine(
        get_new_machine_name(oac),
        "A simple test machine",
        {"weight": {"type": "num", "units": "kg"}},
    )
    machines = oac.get_all_machines()
    for machine in machines:
        if machine["uid"] == new_id:
            return
    raise KeyError("The new machine wasn't on the list")


def test_select_machine():
    oac = OptimyzerClient.from_credentials("secrets/user_credentials.json")

    # Try accessing the machine before selecting one
    with pytest.raises(RuntimeError):
        print(oac.machine.name)

    # Try selecting a non-existent machine
    with pytest.raises(RuntimeError):
        oac.select_machine("not_really_a_uid")

    machines = oac.get_all_machines()
    if not machines:
        # Create a machine for this test
        uid = oac.add_machine(
            get_new_machine_name(oac),
            "A simple test machine",
            {"weight": {"type": "num", "units": "kg"}, "surface": {"type": "cat"}},
        )
    else:
        uid = machines[0]["uid"]

    oac.select_machine(uid)
    print(oac.machine.name)
    print(oac.machine.description)
    print(oac.machine.created_on)
    print(oac.machine.get_all_optimizations())
