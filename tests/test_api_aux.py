# Copyright (c) 2020-2023, Gauss Machine Learning GmbH. All rights reserved.
# This file is part of the Optimyzer API Client, which is released under the BSD 3-Clause License.

# type: ignore
# pylint: disable=missing-module-docstring
# pylint: disable=protected-access

import json
import pytest
import requests  # type: ignore
from typing import Any, Dict, Optional


def mock_response(status_code: int, content: Optional[Dict[str, Any]] = None) -> requests.Response:
    """
    Return a mock Response object with the desired status code and content.
    """
    response = requests.Response()
    response.status_code = status_code
    if content is None:
        response._content = json.dumps({"detail": None}).encode("utf-8")
    else:
        response._content = json.dumps(content).encode("utf-8")
    return response


def test_check_response() -> None:
    from optimyzer_api_client.api_connection import check_response

    res_200 = mock_response(200)
    res_401 = mock_response(401)
    res_500 = mock_response(500)

    # Check that the default response passes
    check_response(res_200)

    # Check that it fails when the expected response is different
    with pytest.raises(RuntimeError):
        check_response(res_200, expected_code=201)

    # Check that a PermissionError is raised for a 401 response
    with pytest.raises(PermissionError):
        check_response(res_401)

    # Check that a RuntimeError is raised for a 500 response
    with pytest.raises(RuntimeError):
        check_response(res_500)


def test_error_handling() -> None:
    from optimyzer_api_client.api_connection import handle_error

    res_400_detail_str = mock_response(400, {"detail": "Bad Request"})
    res_400_detail_dict = mock_response(400, {"detail": {"type": "1", "msg": "Error"}})
    res_400_detail_list = mock_response(
        400, {"detail": [{"type": "1", "msg": "Error"}, {"type": "2", "msg": "Another error"}]}
    )
    res_401 = mock_response(401)
    res_404 = mock_response(404)
    res_500 = mock_response(500)

    # Check the response for 401
    with pytest.raises(RuntimeError, match="The API token timed out!"):
        handle_error(res_401)

    # Check the response for 500
    with pytest.raises(RuntimeError):
        handle_error(res_500)

    # Check a response without details
    with pytest.raises(RuntimeError):
        handle_error(res_404)

    # Check a response with string detail
    with pytest.raises(RuntimeError):
        handle_error(res_400_detail_str)

    # Check a response with dict detail
    with pytest.raises(RuntimeError):
        handle_error(res_400_detail_dict)

    # Check a response with many details
    with pytest.raises(RuntimeError):
        handle_error(res_400_detail_list)
