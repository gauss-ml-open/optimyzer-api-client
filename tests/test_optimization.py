# Copyright (c) 2020-2023, Gauss Machine Learning GmbH. All rights reserved.
# This file is part of the Optimyzer API Client, which is released under the BSD 3-Clause License.

# type: ignore
# pylint: disable=missing-module-docstring
# pylint: disable=protected-access

import numpy as np
import pytest

from optimyzer_api_client import OptimyzerClient
from optimyzer_api_client.optimization import Setting, SettingType


def create_machine(oac: OptimyzerClient):
    machines = oac.get_all_machines()
    for machine in machines:
        if machine["name"] == "Machine 4":
            oac.select_machine(machine["uid"])
            return

    mach_id = oac.add_machine(
        "Machine 4", "A simple test machine", {"weight": {"type": "num", "units": "kg"}}
    )
    oac.select_machine(mach_id)


def create_optimization(
    oac: OptimyzerClient,
    wrong_variables: bool = False,
    no_settings: bool = False,
    no_measurements: bool = False,
    no_goals: bool = False,
    wrong_goals: bool = False,
):
    if wrong_variables:
        variables = {"height": 3}
    else:
        variables = {"weight": 3}

    if no_settings:
        settings = []
    else:
        settings = [
            Setting("force", SettingType.FLOAT, [50, 150], units="N"),
            Setting("duration", SettingType.DISCRETE, [2, 2.5, 3, 3.5, 4], units="seconds"),
        ]

    if no_measurements:
        measurements = {}
    else:
        measurements = {"position error": {"values": [0, 2]}}

    if no_goals:
        goal_weights = {}
    else:
        goal_weights = {"force": -0.1, "position error": (-1, {"duration": -0.1})}
    if wrong_goals:
        goal_weights.update({"accuracy": 1})

    new_opt_id = oac.machine.add_optimization(
        name="Model #1",
        description="FF push to position with force and duration",
        variables=variables,
        settings=settings,
        measurements=measurements,
        goal_weights=goal_weights,
    )
    oac.machine.select_optimization(new_opt_id)


def test_create_optimization():
    # Create a client instance
    oac = OptimyzerClient.from_credentials("secrets/user_credentials.json")

    # Create a machine and select it
    create_machine(oac)

    with pytest.raises(RuntimeError):
        # Try to access an optimization before selecting one
        print(oac.machine.optimization)

    with pytest.raises(RuntimeError):
        # Try selecting a non-existent optimization
        oac.machine.select_optimization("not_really_a_uid")

    with pytest.raises(KeyError):
        # Try creating an optimization without the right variables
        create_optimization(oac, wrong_variables=True)

    with pytest.raises(ValueError):
        # Try creating an optimization without passing any settings
        create_optimization(oac, no_settings=True)

    with pytest.raises(ValueError):
        # Try creating an optimization without passing any measurements
        create_optimization(oac, no_measurements=True)

    with pytest.raises(ValueError):
        # Try creating an optimization without passing any goals
        create_optimization(oac, no_goals=True)

    with pytest.raises(RuntimeError):
        # Try creating an optimization with non-existent goal parts
        create_optimization(oac, wrong_goals=True)

    # Add a proper optimization
    create_optimization(oac)


def test_report_results():
    oac = OptimyzerClient.from_credentials("secrets/user_credentials.json")
    # Create a machine and select it
    create_machine(oac)
    # Create an optimization and select it
    create_optimization(oac)

    optimization = oac.machine.optimization

    with pytest.raises(RuntimeError):
        # Try reporting a result without the right settings
        optimization.report_results({"force": 100}, {"position error": 0.5})

    with pytest.raises(RuntimeError):
        # Try reporting a result without the right measurements
        optimization.report_results({"force": 100, "duration": 3.5}, {"position": 1.2})


def test_run_optimization():
    oac = OptimyzerClient.from_credentials("secrets/user_credentials.json")
    # Create a machine and select it
    create_machine(oac)
    # Create an optimization and select it
    create_optimization(oac)

    optimization = oac.machine.optimization
    print(optimization)

    num_exp = 5
    for i in range(num_exp):
        # Get a suggestion including extra info
        suggestion = optimization.get_suggestion(include_prediction=True, include_nearest=True)
        settings = {key: suggestion[key] for key in ["force", "duration"]}
        if i == num_exp - 1:
            # Report a zero error in the last experiment
            # This experiment should then be the best result
            pos_error = 0
        else:
            pos_error = 0.25 + np.random.rand()
        results = {"position error": pos_error}
        optimization.report_results(settings, results, f"Experiment #{i}")

    data = optimization.get_data()
    for point in data:
        print(point["settings"])
        print(point["results"])
        print("Score:", point["score"])
    assert len(data) == num_exp

    best = optimization.get_best()
    assert best["settings"]["force"] == pytest.approx(settings["force"], rel=1e-3, abs=1e-3)
